var oClass;
var dMap = null;
var mapPoly = new Array();
var dPoly = null;
var edObjectClass = null; // Для редакции - установка типа объекта
var edObjectId = null; // Для редакции - установка типа объекта
var edRegion2 = null; // Для редакции - Регион
var edRegion3 = null; // Для редакции - Регион
var isNewbuild = 0; // Флаг говорящий, что это форма новостройки
var filterStr = null; // Строка для возврата на ту же страницу фильтра в объектах
var hId = null;
// Инициализация формы
function formInit() {
    ymInit({markerDraggable: true});

    edRegionSet(true);
    
    $('#street').autocomplete(urlpath + 'data/streetsearch_r.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        matchContains: false,
        matchSubset: false,
        delay: 30,
        formatItem: formatStreet,
        max: 600,
        scroll: true,
        extraParams: {
            'k': function(){return getRegion();}
        },
        onItemSelect: function(v){
            $('#street').val(v);
            checkStreet();
            $('[name="house_num"]').val('');$('#house_num').flushCache();
            isStreetNeeded();
        }
    });
    
    $('#house_num').autocomplete(urlpath + 'data/housesearch.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        max: 400,
        scroll: true,
        extraParams: {
            'k': function(){
                return getRegion();
            },
            's': function(){
                return $('#street').val();
            }
        },
        onItemSelect: function(v){
            checkHouse();isStreetNeeded();
        
        }
    });
    $('#house_num').change(function(){checkHouse();});
    
    objTypeChange(true);
    getRegionCallback = function(rr){
        isStreetNeeded(rr);
        getOrientirs(rr);
    };
    if ($('[name=bannerActive]').attr('checked')) 
        $('#bannerBlock').show();
    else 
        $('#bannerBlock').hide();
    
    if ($('[name=sliderActive]').attr('checked')) 
        $('#sliderBlock').show();
    else 
        $('#sliderBlock').hide();
        
   if ($('[name=newbuildPlans]').val())
      loadNewbuildPlans(); 
   if ($('[name=newbuildChrono]').val())
      loadNewbuildChrono();
   
   if (!jQuery.browser.msie)
      $('[name=adv_info_pub]').markItUp(mySettings);      
   
   chkSendPrice();   
   $('[name=type1], [name=type2], [name=n_elite], [name=price_class], [name=region1], [name=region2], [name=region3], [name=is_publish]').change(function(){
      chkSendPrice();
    });
    $('[name="house_num"], [name="regionCustom"], [name=region1], [name=region2], [name=region3]').change(function(){
        isStreetNeeded();
    });
   
   
   $('[name=n_elite]').attr('title', 'Эта опция - платная!').tipTip({delay: 0});
   $('[name=price_class][value=3]').attr('title', 'В некоторых регионах эта опция - платная!').tipTip({delay: 0});
   //checkHouse();
    $(".images_box li .iNum").each(function(index){
        $(this).text(index + 1);
    });
   $(".images_box img").load(function(){
       $(".images_box").shapeshift({
            enableDragAnimation: false,
            paddingY: 20
        });
        $(".images_box").on("ss-event-dragged ss-event-dropped ss-event-destroyed", function(e, selected) {
            console.log(321);
            $(".images_box li .iNum").each(function(index){
                $(this).text(index + 1);
            });
        });

    });
   /*$('#regionTown').ajaxChosen({
        type: 'GET',
        url: '/data/searchTown.json',
        dataType: 'json'
    }, function (data) {
        var results = [];

        $.each(data, function (i, val) {
            results.push({ value: val.value, text: val.text });
        });
        console.log(results);
        return results;
    });*/
    
    $('[name=bannerActive]').change(function() {
        if ($(this).is(':checked')) {
           
            $('#bannerBlock').show();
        } else {
            $('#bannerBlock').hide();
        }
    });
    
    $('[name=sliderActive]').change(function() {
        if ($(this).is(':checked')) {
           
            $('#sliderBlock').show();
        } else {
            $('#sliderBlock').hide();
        }
    });
    
    $('[name=confirmrules]').change(function(){
        if($(this).is(':checked')){
          $('#buttonz').show();
        } else {
          $('#buttonz').hide();
        }
      });
   
   
    getPriceCur();
   
   
   }
   
function getPriceCur() {
    
    var price = $('[name=price]').val();
    var price_class = $('[name=price_class]').filter(":checked").val();
    var price_exch = $('[name=price_exch]').val();
    $('.currency-panel-cur').empty();
    $.post('/cabinet/getPriceCur.json', {price: price, price_class: price_class, price_exch: price_exch}, function(data){
        $('.currency-panel-cur').empty();
        for (i in data) {
             $('.currency-panel-cur').append('<span>' + data[i] + '</span>');
         }
    }, 'json');
    
    $('[name=price]').keyup(function(){
       var price = $('[name=price]').val();
       var price_class = $('[name=price_class]').filter(":checked").val();
       var price_exch = $('[name=price_exch]').val();
       $('.currency-panel-cur').empty().css({height: "54px", width: "270px"});
       $.post('/cabinet/getPriceCur.json', {price: price, price_class: price_class, price_exch: price_exch}, function(data){
           $('.currency-panel-cur').empty().css({height: "54px", width: "270px"});
           for (i in data) {
                $('.currency-panel-cur').append('<span>' + data[i] + '</span>');
            }
       }, 'json');
   });
   $('[name=price_class], [name=price_exch]').on('change', function(){
       var price = $('[name=price]').val();
       var price_class = $('[name=price_class]').filter(":checked").val();
       var price_exch = $('[name=price_exch]').val();
       $('.currency-panel-cur').empty().css({height: "54px", width: "270px"});
       $.post('/cabinet/getPriceCur.json', {price: price, price_class: price_class, price_exch: price_exch}, function(data){
           $('.currency-panel-cur').empty().css({height: "54px", width: "270px"});
           for (i in data) {
                $('.currency-panel-cur').append('<span>' + data[i] + '</span>');
            }
       }, 'json');
   });
}   
   
function formInitHouse() {
    if (typeof yMapUserGeo !== "undefined") {
        ymInit({
            markerDraggable: true,
            center: yMapUserGeo
        });
    } else {
        ymInit({markerDraggable: true});
    }

    edRegionSet(true);
    
    $('#street').autocomplete(urlpath + 'data/streetsearch_r.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        matchContains: false,
        matchSubset: false,
        delay: 30,
        formatItem: formatStreet,
        max: 600,
        scroll: true,
        extraParams: {
            'k': function(){return getRegion();}
        },
        onItemSelect: function(v){
            $('#street').val(v);
            checkStreet();
            $('[name="house_num"]').val('');$('#house_num').flushCache();
            isStreetNeeded();
        }
    });
    
    $('#house_num').autocomplete(urlpath + 'data/housesearch.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        max: 400,
        scroll: true,
        extraParams: {
            'k': function(){
                return getRegion();
            },
            's': function(){
                return $('#street').val();
            }
        },
        onItemSelect: function(v){
            checkHouse();isStreetNeeded();
        
        }
    });
    $('#house_num').change(function(){checkHouse();});
    
   
}

function chkSendPrice()
   {
   $('#objPriceInfo').html('');
   $('[name=price_class][value=3]').attr('title', '');
   $('#objPriceInfoWrp').hide();
   $.get(sectionUrl+'ajax', {
      'checkBillSvc': 1,
      'id': $('[name=editId]').val(),
      'region': getRegion(),
      'type': parseInt($('#type1 option:selected').val()) + parseInt($('#type2 option:selected').val()),
      'n_elite': $('[name=n_elite]:checked').val(),
      'is_publish': $('[name=is_publish]:checked').val(),
      'price_class': $('[name=price_class]:checked').val()
   }, function(data){
      $('#objPriceInfo').html(data.name+': цена - '+data.price+' грн');
      if (data.price>0)
         {
         $('#objPriceInfoWrp').show();
         }
      }, 'json');
   }

// Исправляет улицу из ошибки "Возможно вы имели в виду"
function correctStreet(s){
    $('#street').val(s);
    checkStreet();
}

// Отправляет улицу на проверку
function checkStreet(){
    if ($('#street').isCompleterOpen()) return;
    
    $('#streetWarn').removeClass('streetWarn');
    $('#streetWarn').html('');
    $.get(sitePath + 'data/checkStreet.json', {
        'r': getRegion(),
        's': $('#street').val(),
        'rnd': Math.random()
    }, function(data){
        if (data) {
            if (data.autoSet == 1) {
                $('#street').val(data.name);
            }
            else {
                $('#streetWarn').addClass('streetWarn');
                err = new Array();
                for (key in data.s) {
                    err[key] = '<a href="javascript:void(0);" onClick="correctStreet(\'' + data.s[key].name + '\');"><nobr>' +
                    data.s[key].name +
                    '</nobr></a>';
                }
                if (data.error) {
                    $('#streetWarn').html('<div id="streetError"><b>Ошибка!</b> ' + data.error + '</div>');
                }
                else {
                    $('#streetWarn').html('<div id="streetError"><b>Не удалось найти такую улицу.</b> Проверьте, возможно, Вы имели в виду:<br>' + err.join(', ') + '<br>Если ошибки нет - игнорируйте данное сообщение</div>');
                }
            }
        }
    }, 'json');
}


// Отправляет дом на проверку наличия карты и фоток
function checkHouse() {
    if ($('#house_num').isCompleterOpen()) return; 
    
    $('#lFotkiH > li[autoloaded="1"]').remove();

    $('#houseWarn').removeClass('streetWarn');
    $('#houseWarn').html('');
    $('#existsPhotos').html('');
    hId = null;
    $('#err').hide();
    $('#house_data').hide();
    $('#houseError').html('');
    $.get(sitePath + 'data/houseMapCheck.json', {
        'r': getRegion(),
        's': $('#street').val(),
        'h': $('#house_num').val(),
        'rnd': Math.random()
    }, function(data){
        $('.video_block').empty();
        $('#lblVideoH, #lblComment').hide();
        if (data) {
            hId = data.hid;
            if (data.isMap!=1)
            if (hId != 0) {
                $('#err').show();
                $('#house_data').show();
                $('#houseError').html('Укажите № дома и/или объект на КАРТЕ, для участия в Поиске на карте &#8594;');
                for (var key in data.house_data) {
                    if (data.house_data[key] == null) {
                        $('#hd_' + key).html(' -');
                    }
                    else {
                        $('#hd_' + key).html(' ' + data.house_data[key]);
                    }
                }
            }
            if ('house_data' in data && data['house_data']) {
                if ('b_year' in data['house_data'] && data['house_data']['b_year']) {
                    $('#built_year').val(data['house_data']['b_year']);
                }
                if ('ceil' in data['house_data'] && data['house_data']['ceil']) {
                    $('#ceil_height').val(data['house_data']['ceil']);
                }
                if ('storey' in data['house_data'] && data['house_data']['storey']) {
                    $('#house_high').val(data['house_data']['storey']);
                }
                if ('wall_type' in data['house_data'] && data['house_data']['wall_type']) {
                    $('#wall_type option:contains("' + data['house_data']['wall_type'] + '")').attr('selected', 'selected')
                }
                $('#houseIDNum').empty().html('<a target="_blank" href="/cabinet/moder-address.html?menu=all&search=' + hId + '">' + hId + '</a>');
            }
            if (data.isPhotos == 1) {
                $('#adress_label').text(data.adr_v);
                $('#lFotkiH li[autoloaded="1"]').remove();
                for (i in data.photos) { 
                    loadedPhoto(data.photos[i].i, 'H', 3, {
                        autoloaded: 1
                    });
                }
                
                if(data.house_data.youtube) {
                    $('#lblVideoH').show();
                    for(i in data.house_data.youtube) {
                        iframe = '<iframe style="margin:10px 0px " width="410" height="240"  src="https://www.youtube.com/embed/' + data.house_data.youtube[i] + '"  frameborder="0" allowfullscreen></iframe>';
                        $('.video_block').append(iframe);
                        links = '<p><span>Ссылка на это видео на Youtube: </span><br><a rel="nofollow" target="_blank" href="https://www.youtube.com/watch?v='+ data.house_data.youtube[i]+'"> https://www.youtube.com/watch?v='+ data.house_data.youtube[i]+'</a></p>'+
                                '<a class="link-for-comment-house" target="_blank" href="https://www.youtube.com/watch?v='+ data.house_data.youtube[i]+'"" rel="nofollow">Хотите оставить комментарий к видео?</>';
                                //'<p><a target="_blank" href="">Ссылка на видеообзор района{$val.bla} </a></p>'+
                                //'<p><a target="_blank"  href="https://www.youtube.com/embed/'+ data.house_data.youtube[i]+'">Смотреть в полноэкранном режиме</a></p>';
                        $('.block-for-links').empty().append(links);
                    
                    }
                    $('#lblComment').show();
                    $('.comment_obj_block').empty().append('<a target="_blank" class="link-for-comment-house" href="/poleznoe/photoalbum/'+ data.house_data.url+'-'+data.hid+'.html#aComments"" rel="nofollow"><span class="comments">'+data.house_data.comm_cnt+'</span>Хотите написать отзыв о доме?</>');
                }
                
            }
        }
    }, 'json');
}


function isStreetNeeded(){
    needStreet = false;
    needHouse = false;
    t = $('#type2').val();
    
    if (getRegionType == 'М' || getRegionType == 'Р' || getRegionType == 'А') {
        needStreet = true;
        if (t == 0) 
            needHouse = true;
    }
    
    if (needStreet) 
        $('#needStreet').show();
    else 
        $('#needStreet').hide();
    
    if (needHouse) 
        $('#needHouse').show();
    else 
        $('#needHouse').hide();
    
    geocodePlace();
}

function objTypeChange(isAddEmpty){
    if (isNewbuild == 1) {
        return 0;
    }
    
    t1 = $('#type1').val();
    t2 = $('#type2').val();
    
    if (t1 == 0 && t2 != 4 && t2 != 6) 
        $("#price_class0").attr('checked', 'checked');
    else 
        if (t2 == 4) 
            $("#price_class0").attr('checked', 'checked');
        else 
            if (t2 == 6) 
                $("#price_class4").attr('checked', 'checked');
            else 
                $("#price_class2").attr('checked', 'checked');
    
    $('#object_class').empty();
    if (isAddEmpty) 
        $('#object_class').append('<option value=""> --- Тип ---</option>');
    
    for (key in oClass[t2]) {
        $('#object_class').append('<option value="' + oClass[t2][key].id + '">' + oClass[t2][key].name + '</option>');
    }
    
    
    $('.shAll').hide();
    if (t1 == 0 && t2 == 0) 
        $('.shSaleFlat').show();
    if (t1 == 1 && t2 == 0) 
        $('.shRentFlat').show();
    if (t1 == 0 && t2 == 2) 
        $('.shSaleHouse').show();
    if (t1 == 1 && t2 == 2) 
        $('.shRentHouse').show();
    if (t1 == 0 && t2 == 4) 
        $('.shSaleOffice').show();
    if (t1 == 1 && t2 == 4) 
        $('.shRentOffice').show();
    if (t1 == 0 && t2 == 6) 
        $('.shSaleLand').show();
    if (t1 == 1 && t2 == 6) 
        $('.shRentLand').show();
    
    if (edObjectClass > 0) 
        $('#object_class').val(edObjectClass);
    
    isStreetNeeded();
}

function delPhoto(f, v, u){
    if (confirm('Удалить эту фотографию?')) {
        if (u == 1) {
            $('#lPhoto' + v + f).remove();
            $('#loadPhoto' + v + f).remove();
        }
        else 
            if (u == 2 || u == 3 || u == 4) {
                act = {
                    delHousePhoto: f
                };
                if (u == 2) 
                    act = {
                        delMediaPhoto: f
                    };
                $.get(sectionUrl + 'ajax', act, function(data){
                    if (data.result == 1) {
                        $('#lPhoto' + v + f).remove();
                        $('#loadPhoto' + v + f).remove();
                    }
                    else 
                        alert('Ошибка удаления');
                }, 'json');
            }
    }
    l = $('#lFotki' + v);
    if (l.children().length > 0) 
        $('#lblPhoto' + v).show();
    else 
        $('#lblPhoto' + v).hide();
}

function makePhotoFirst(f, v, u)
   {
   $('#lFotki'+v).find('.iTop').addClass('imgopacitypollitra');
   $('#lPhoto'+v+f).find('.iTop').removeClass('imgopacitypollitra');
   $('[name=makePhotoFirstId]').val(f);
   $('[name=makePhotoFirstType]').val(u);
   }

   
function serializeObjetPhotos() {
    var images_container    = $('#lFotki');
    var images_list = [];
    images_container.find('li img[data-name]').each(function(index){
        images_list.push({
            'name':   $(this).data('name'),
            'ext':    $(this).data('ext')
        });
    });
    var images_json     = $.toJSON(images_list);
    $('textarea[name="images_json"]').val(images_json);
}
function rebuildObjetPhotos() {
    var images_container    = $('#lFotki');
    images_container.shapeshift({
        enableDragAnimation: false,
        paddingY: 20
    });

    images_container.find("li .iNum").each(function(index){
        $(this).text(index + 1);
    });
    images_container.off("ss-event-dragged ss-event-dropped ss-event-destroyed").on("ss-event-dragged ss-event-dropped ss-event-destroyed", function(e, selected) {
        serializeObjetPhotos();
            console.log(456);
        $(".images_box li .iNum").each(function(index){
            $(this).text(index + 1);
        });
    });
    serializeObjetPhotos();
}

function initObjectImagesEvents() {
    var images_container    = $('#lFotki');
    images_container.off("li .iDel").on('click', 'li .iDel', function(){
        if (confirm('Удалить эту фотографию?')) {
            var parent_block    = $(this).parent().parent();
            parent_block.remove();
            rebuildObjetPhotos();
        }
    });
    rebuildObjetPhotos();
}
function loadedObjetPhoto(name, ext, filehash) {
    var images_container    = $('#lFotki');
    var preview             = '/' + name + '_90x0' + ext;
    var appendix            =  '<div class="iNum"></div>';
    // Добавляем в контейнер
    images_container.append('<li id="lPhoto' + filehash + '">' +
    '<div class="iDiv" style="background-image: url(' + preview + ')">' +
        '<img data-name="' + name + '" data-ext="' + ext + '" src="' + preview + '" />' +
        appendix + 
        '<div class="iDel" title="Удалить фотографию"></div>' +
        '</div>' +
    '</li>');
    if (images_container.children().length > 0) {
        $('#lblPhoto').show();
    } else {
        $('#lblPhoto').hide();
    }
    rebuildObjetPhotos();
}
function loadedProjectPhoto(name, ext) {
    var images_container    = $('#main_image');
    var preview             = '/' + name + '_90x0' + ext;
    var appendix            =  '<div class="iNum"></div>';
    // Добавляем в контейнер
    $('[name="main_image_name"]').val(name);
    $('[name="main_image_ext"]').val(ext);
    images_container.html('<img data-name="' + name + '" data-ext="' + ext + '" src="' + preview + '" />');
}
function loadedPhoto(f, v, u, opt){
    if (!opt) 
        opt = {};

    l = $('#lFotki' + v);
    // Для загруженных фото
    if (u == 1) 
        iUrl = sectionUrl + 'photoPreview.json?preview=' + f;
    // Для уже имеющихся в медиабиблиотеке фото
    if (u == 2) 
        iUrl = urlpath + 'data/image/objectedit_media_t/' + f + '.jpg';
    
    // Для уже имеющихся фоток домов
    if (u == 3 || u == 4) 
        iUrl = urlpath + 'data/image/objectedit_photo_t/' + f + '.jpg';
    var appendix = '';
    if (l.hasClass('images_box')) {
        appendix    =  '<div class="iNum">5</div>';
    }
    l.append('<li id="lPhoto' + v + f + '"' + (opt.autoloaded ? ' autoloaded="1"' : '') + '>' +
    '<div class="iDiv" style="background-image: url(' + iUrl + ')">' +
        '<img src="' + iUrl + '" />' +
        '<input value="' + f + '" name="files[]" type="hidden"/>' +
        (u != 3 ? 
            appendix + 
           '<div class="iDel" title="Удалить фотографию" onClick="delPhoto(\'' + f + '\', \'' + v + '\', \'' + u + '\')"></div>'
        : '') +
        '</div>' +
    '</li>');
    if (l.children().length > 0) 
        $('#lblPhoto' + v).show();
    else 
        $('#lblPhoto' + v).hide();
    
    $('#lFotki' + v + ' > .clear').remove();
    c = $('#lFotki' + v).children('li');
    // for (i = 0; i < c.length; ++i) 
        // if (i > 0 && i % 3 == 0) 
            // $('<div class="clear"></div>').insertBefore($(c[i]));
    $(".images_box").shapeshift({
        enableDragAnimation: false,
        paddingY: 20
    });
    
    h = $('#lFotkiH').children('li');
     for (i = 0; i < h.length; ++i) 
         if (i > 0 && i % 3 == 0) 
     $('<div class="clear"></div>').insertBefore($(h[i]));
       $(".images_box img").load(function(){
           $(".images_box").shapeshift({
                enableDragAnimation: false,
                paddingY: 20
            });
        });
    $(".images_box li .iNum").each(function(index){
        $(this).text(index + 1);
    });
    if (v == '') {
            console.log('testt!!!');
        $(".images_box").off("ss-event-dragged ss-event-dropped ss-event-destroyed").on("ss-event-dragged ss-event-dropped ss-event-destroyed", function(e, selected) {
            $(".images_box li .iNum").each(function(index){
                $(this).text(index + 1);
            });
        });
    }
}


function sendFormPopup(nb, cont) {
    if($('[name=price]').val() !== "") {
        $('.send-form-block').show();
    }else {
        sendForm(0);
    }
    //sendForm(nb, cont);
}

function changePrice() {
    $('.send-form-block').hide();
    $(window).scrollTop($('.object_form_price').offset().top-150);
    $('.object_form_price').addClass('errorInput');
}

function sendForm(nb, cont){
    $('.send-form-block').hide();
    adm = parseInt($('[name=adminId]').val());
    postUrl = 'postObject.json';
    if (nb == 1) {
       postUrl = 'postNewbuild.json';
       saveNewbuildPlan();
       saveNewbuildChrono();
   }
    
    $('#drawedMap').val('');
    if (yMapUserGeo.set == 1) 
        $('#drawedMap').val(yMapUserGeo.lat + ',' + yMapUserGeo.lng);
    
    postJsonForm($('#objForm'), postUrl, null, function(data){
        if (edObjectId > 0) {
            if (nb == 1) 
                document.location = sectionUrl + 'newbuilds.html' + (adm > 0 ? '?adminId=' + adm : '');
            else 
                if ($('[name=edProjectLink]').val() > 0) 
                    document.location = sectionUrl + 'objects-nb.html' + (filterStr != null ? '?filterStr=' + filterStr + (adm > 0 ? '&adminId=' + adm : '') : (adm > 0 ? '?adminId=' + adm : ''));
                else 
                    document.location = sectionUrl + 'objects.html' + (filterStr != null ? '?filterStr=' + filterStr + (adm > 0 ? '&adminId=' + adm : '') : (adm > 0 ? '?adminId=' + adm : ''));
        }
        else {
            if (nb == 1) 
                document.location = sectionUrl + 'newbuilds.html' + (adm > 0 ? '?adminId=' + adm : '');
            else 
                if ($('[name=nbProjectLink]').val() > 0 && cont != 1) 
                    document.location = sectionUrl + (filterStr != null ? 'objects-nb.html?filterStr=' + filterStr + (adm > 0 ? '&adminId=' + adm : '') : 'newbuilds.html' + (adm > 0 ? '?adminId=' + adm : ''));
                else 
                    if ($('[name=nbProjectLink]').val() > 0 && cont == 1) 
                        document.location = document.location;
                    else 
                        document.location = sectionUrl + 'objects.html?' + (adm > 0 ? 'adminId=' + adm + '&' : '') + 'newObject=true#bannerTop';
        }
    });
}

function taLength(t, l){
    if ($(t).val().length > l) 
        $(t).val($(t).val().substr(0, l));
}

// *** Newbuilds functions
function addNewbuildPlan(obj)
   {
   o=$('#planTemplate').clone();
   o.attr('id', '');
   o.attr('plItem', '1');
   if (obj)
      {
      for (i in obj)
         {
         o.find('[name='+obj[i].name+']').val(obj[i].value);
         if (obj[i].name=='image')
            o.find('[name=photoPreview]').attr('src', obj[i].value);
         if (obj[i].name=='id')
            o.find('a[name=id]').attr('val', obj[i].value);
         }
      }

   new qq.FileUploader({
          element: o.find('[name=planPhoto]')[0],
          inputId: 'planPhoto',
          action: urlpath+'data/image_upload.json',
          multiple: false,
          rowElement: o,
          onComplete: function(id, fileName, response){ if(response.success)
             {
             this.rowElement.find('[name=photoPreview]').attr('src', sectionUrl+'photoPreview.json?preview='+response.filehash);
             }}
          });
   
   o.find('[name=description]').markItUp(mySettings)
    
   $('#plansList').append( o );
   o.show();
   }
   
function saveNewbuildPlan()
   {
   lst=$('#plansList').find('[plItem=1]').find('form');
   rv=[];
   for (i=0; i<lst.length; ++i)
      rv.push( $(lst[i]).serializeArray() );
   $('[name=newbuildPlans]').val($.toJSON(rv));
   }

function delNewbuildPlan(o)
   {
   l=$('#plansList > div');
   for (i=0; i<l.length; ++i )
      {
      if ($(l[i]).find('input[name=id]').val()==o.attr('val'))
         {
         el=$(l[i]);
         $.get(sectionUrl+'ajax', {'deleteNewbuildPlan': o.attr('val')}, function(data){
            el.detach();
            }, 'json');
         }
      }
   }

function loadNewbuildPlans()
   {
   pl=$.evalJSON($('[name=newbuildPlans]').val());
   for (i in pl)
      addNewbuildPlan(pl[i]);
   }


function addNewbuildChrono(obj)
   {
   o=$('#chronoTemplate').clone();
   o.attr('id', '');
   o.attr('plItem', '1');
   if (obj)
      {
      for (i in obj)
         {
         o.find('[name='+obj[i].name+']').val(obj[i].value);
         if (obj[i].name=='image')
            o.find('[name=photoPreview]').attr('src', obj[i].value);
         if (obj[i].name=='id')
            o.find('a[name=id]').attr('val', obj[i].value);
         }
      }

   new qq.FileUploader({
          element: o.find('[name=planPhoto]')[0],
          inputId: 'planPhoto',
          action: urlpath+'data/image_upload.json',
          multiple: false,
          rowElement: o,
          onComplete: function(id, fileName, response){ if(response.success)
             {
             this.rowElement.find('[name=photoPreview]').attr('src', sectionUrl+'photoPreview.json?preview='+response.filehash);
             }}
          });
   
   $('#chronoList').append( o );
   o.show();
   }
   
function saveNewbuildChrono()
   {
   lst=$('#chronoList').find('[plItem=1]').find('form');
   rv=[];
   for (i=0; i<lst.length; ++i)
      rv.push( $(lst[i]).serializeArray() );
   $('[name=newbuildChrono]').val($.toJSON(rv));
   }

function loadNewbuildChrono()
   {
   pl=$.evalJSON($('[name=newbuildChrono]').val());
   for (i in pl)
      addNewbuildChrono(pl[i]);
   }

function delNewbuildChrono(o)
   {
   l=$('#chronoList > div');
   for (i=0; i<l.length; ++i )
      {
      if ($(l[i]).find('input[name=id]').val()==o.attr('val'))
         {
         el=$(l[i]);
         $.get(sectionUrl+'ajax', {'deleteNewbuildChrono': o.attr('val')}, function(data){
            el.detach();
            }, 'json');
         }
      }
   }

var ooLastRegion=null;
function getOrientirs(rr)
   {
   if (ooLastRegion==rr) return;
   ooLastRegion=rr;
   $('#orientirBlock').hide();
   $.get(urlpath+'data/orientirs.json', {'r':rr}, function(data){
      if (data.cnt>0)
         {
         $('#orientirBlock').show();
         $('#orientir').html('');
         $('#orientir').append('<option value=""> --- </option>');            
         for (i in data.rr)
            $('#orientir').append('<option value="'+data.rr[i].id+'">'+data.rr[i].name+'</option>');            
         if ($('[name=sOrientir]').val()>0)
            $('#orientir').val( $('[name=sOrientir]').val() );
         }
      }, 'json');
   }

// *** Картография - Yandex Maps
var yMap = null;
var yMapMarker = null;
var yMapLine = new Array();
var yMapStyleRed = null;
var yMapStyleGreen = null;
var yMapUserGeo = {};
var yMapDefPrm = {};
function ymInit(prm){
    if (!isset(prm)) {prm={};}
    if (!isset(prm.div)) prm.div='yMap';
    //if (!isset(prm.center)) prm.center={lng:30.52369, lat: 50.45004};
    if (!isset(prm.center) || !isset(prm.center.lng) || !isset(prm.center.lat)) prm.center={lng:30.52369, lat: 50.45004};
    if (!isset(prm.zoom)) prm.zoom=9;
    if (!isset(prm.markerDraggable)) prm.markerDraggable=false;
    yMapDefPrm=prm;
    if ($('#' + prm.div).length==0) {
		return false;
	}
    // *** Проинициализируем карту
    yMap = new YMaps.Map(document.getElementById(prm.div), {
        ignoreResize: false
    });
    yMap.setCenter(new YMaps.GeoPoint(prm.center.lng, prm.center.lat), prm.zoom);
    yMap.addControl(new YMaps.TypeControl());
    if (!prm.noToolBar)
       yMap.addControl(new YMaps.ToolBar());
    yMap.addControl(new YMaps.Zoom());
    yMap.disableDblClickZoom();
    //yMap.addControl(new YMaps.MiniMap());
    yMap.addControl(new YMaps.ScaleLine());
    yMap.enableScrollZoom();
    
    // *** Добавим маркер
    yMapMarker = new YMaps.Placemark(new YMaps.GeoPoint(prm.center.lng, prm.center.lat), {
        draggable: prm.markerDraggable,
        hasBalloon: false
    });
   
    if (!prm.noMarker)
       yMap.addOverlay(yMapMarker);

    // *** Стили
    yMapStyleGreen = new YMaps.Style();
    yMapStyleRed = new YMaps.Style();
    yMapStyleRedKrestik = new YMaps.Style();
    yMapStyleGreen.iconStyle = new YMaps.IconStyle();
    yMapStyleRed.iconStyle = new YMaps.IconStyle();
    yMapStyleRedKrestik.iconStyle = new YMaps.IconStyle();
    yMapStyleGreen.iconStyle.href = images + 'map/yMapIco1.png';
    yMapStyleRed.iconStyle.href = images + 'map/yMapIco2.png';
    yMapStyleRedKrestik.iconStyle.href = images + 'map/krestik.png';
    yMapStyleGreen.iconStyle.size = new YMaps.Point(27, 26);
    yMapStyleRed.iconStyle.size = new YMaps.Point(27, 26);
    yMapStyleRedKrestik.iconStyle.size = new YMaps.Point(16, 16);
    
    yMapMarker.setStyle(yMapStyleGreen);
    if (prm.krestik)
       yMapMarker.setStyle(yMapStyleRedKrestik);
    
    YMaps.Events.observe(yMap, yMap.Events.DblClick, function(m, e){
        if (yMapMarker.getOptions().draggable) {
            yMapMarker.setGeoPoint(new YMaps.GeoPoint(e.getCoordPoint().getX(), e.getCoordPoint().getY()));
            yMarkerDrag(e.getCoordPoint().getX(), e.getCoordPoint().getY());
        }
    });
    
    YMaps.Events.observe(yMapMarker, yMapMarker.Events.DragEnd, function(m){
        yMarkerDrag(m.getGeoPoint().getX(), m.getGeoPoint().getY());
    });
}

function yMarkerDrag(x, y){
    yMapUserGeo = {
        lng: x,
        lat: y,
        set: 1
    };
    $('#yMapCancelDrag').show();

    $.get(sitePath + 'data/geowhatshere.json', yMapUserGeo, function(data){
        $('#geoWhatsHere').html('');
        if (data.found) {
            $('#geoWhatsHere').html('Тут находится: ' +
            '<a href="javascript:void(0);" onClick="ymSetAddress(\'' +
            data.region1 +
            '\', \'' +
            data.region2 +
            '\', \'' +
            data.region3 +
            '\', \'' +
            data.street +
            '\', \'' +
            data.house_num +
            '\');">' +
            data.full_name +
            '</a>');
        }
    }, 'json');
}

// *** Установка адреса из WhatsHere
function ymSetAddress(kl1, kl2, kl3, strt, hsnum){
    $('#street').val(strt);
    $('#house_num').val(hsnum);
    $('#region1').val(kl1);
    edRegion2 = kl2;
    edRegion3 = kl3;
    edRegionSet(true);
    if (yMapExpanded) 
        ymExpandMap();
}

// *** Дает запрос на геокодинг текущего местоположения объекта
function geocodePlace(){
    if (!yMap) 
        return;
    
    $.get(sitePath + 'data/geocode.json', {
        region: getRegion(0, 1),
        street: $('#street').val(),
        house: $('#house_num').val()
    }, function(data){
        if (!data) 
            return;
        if (yMapUserGeo.set == 1 && data.adr != 1) {
            yMap.setCenter(new YMaps.GeoPoint(yMapUserGeo.lng, yMapUserGeo.lat), 15);
            yMapMarker.setGeoPoint(new YMaps.GeoPoint(yMapUserGeo.lng, yMapUserGeo.lat));
            return;
        }
        
        yMapUserGeo = {};
        $('#yMapCancelDrag').hide();
        if (data.point) {
            yMap.setCenter(new YMaps.GeoPoint(data.point.lng, data.point.lat), data.scale);
            yMapMarker.setGeoPoint(new YMaps.GeoPoint(data.point.lng, data.point.lat));
        }
        if (data.line) {
            var s = new YMaps.Style();
            s.lineStyle = new YMaps.LineStyle();
            s.lineStyle.strokeColor = 'FF000080';
            s.lineStyle.strokeWidth = '5';
            YMaps.Styles.add("domik#streetLine", s);
            
            for (i in yMapLine) 
                yMap.removeOverlay(yMapLine[i]);
            yMapLine = new Array();
            
            for (i in data.line) {
                yMapLine[i] = new YMaps.Polyline();
                for (i1 in data.line[i]) 
                    yMapLine[i].addPoint(new YMaps.GeoPoint(data.line[i][i1].lng, data.line[i][i1].lat));
                yMapLine[i].setStyle('domik#streetLine');
                yMap.addOverlay(yMapLine[i]);
            }
        }
        if (data.adr == 1) {
            yMapMarker.setStyle(yMapStyleRed);
            yMapMarker.setOptions({
                draggable: false
            });
        }
        else {
            yMapMarker.setStyle(yMapStyleGreen);
            yMapMarker.setOptions({
                draggable: true
            });
        if ($('#street').val())
           $('#houseError').html('Укажите № дома и/или объект на КАРТЕ, для участия в Поиске на карте &#8594;');
        }
    }, 'json');
}

function yMapCancelDrag(){
    yMapUserGeo = {};
    geocodePlace();
    $('#yMapCancelDrag').hide();
}

var yMapExpanded = false;
function ymExpandMap(){
    $('#mapWrapper').css('top', '0');
    $('#mapWrapper').css('left', '0');
    
    yMapExpanded = !yMapExpanded;
    if (yMapExpanded) {
        $('#mapWrapper').css('background', '#FFF');
        $('body').append($('#mapWrapper'));
        $('#mapWrapper').css('position', 'fixed');
        $('#mapWrapper').css('width', ($(window).width()) + 'px');
        $('#mapWrapper').css('height', ($(window).height()) + 'px');
        $('#yMap').css('width', $(window).width() + 'px');
        $('#yMap').css('height', ($(window).height() - 60) + 'px');
        $('#aExpandMap').html('Свернуть обратно');
    }
    else {
        $('#mapWrapperParent').append($('#mapWrapper'));
        $('#mapWrapper').css('position', 'relative');
        $('#mapWrapper').css('width', '');
        $('#mapWrapper').css('height', '');
        $('#yMap').css('width', '298px');
        $('#yMap').css('height', '500px');
        $('#aExpandMap').html('Развернуть карту');
    }
    
    yMap.redraw();
}

function ymExpandMapForm(){
    $('#mapWrapper').css('top', '0');
    $('#mapWrapper').css('left', '0');
    
    yMapExpanded = !yMapExpanded;
    if (yMapExpanded) {
        $('#yMap').css('height', ($(window).height() - 60) + 'px');
        $('#aExpandMap').html('>> Свернуть обратно <<');
    }
    else {
        $('#yMap').css('height', '300px');
        $('#aExpandMap').html('<< Развернуть карту >>');
    }
    
    yMap.redraw();
}

function ymShowPoint(lat, lng, zoom)
   {
   yMap.setCenter(new YMaps.GeoPoint(lng, lat), zoom);
   yMapMarker.setGeoPoint(new YMaps.GeoPoint(lng, lat));
   }
$(document).ready(function(){
        
    $('.show-newbuild-list').on('click', function() {
        $('.carousel-nb-list').toggle();
        $('.carousel-nb-list a').off().on('click', function(){
            $.get('/index/jcarousel.ajax', 
            {
                region: $(this).data('region')
            },
            function(data){

                $('#mycarouselIndex ul').empty();

            },'json');
            return false; 
        });
    });
    
    $('.index_news_region').on('click', function() {
        $('#' + $(this).data('block')).toggle();
    });

    $('.index_news_region_block').hover(function(){
        $('#' + $(this).data('block')).show();
    }, function(){
        $('#' + $(this).data('block')).hide();
    });
    
    $('.index_news_region_block a').on('click', function(){
        var news_region = $(this).data('region');
        var news_chapters = $(this).data('chapters');
        var link = $(this).data('block') + '-' + $(this).data('link');
        var title = $(this).data('title');
        var nr = $(this).data('nr');
        var block = $(this).data('block');
        $.post('/index/get_region_news.ajax', {region: news_region, chapters: news_chapters}, function(data) {
            if(!data.error) {
                $('#news-' + news_chapters).empty();
                for(var i in data) {
                    if(data[i].comments_cnt > 0)
                        var comments = '<span class="comment"><img alt="" src="http://domik.ua/images/ico_comment.gif">' + data[i].comments_cnt + '</span>';
                    else
                        var comments = '';
                    var html = '<li><div class="list_itm_news_time"><span>' + data[i].time + '</span></div><div class="list_itm_news_text"><a target="_blank" title="" href="/novosti/' + data[i].url + '-n' + data[i].id + '.html">' + data[i].title + '</a>' + comments + '</div></li>'
                    $('#news-' + news_chapters).append(html);
                }
                $('#wrap_' + block + ' i').html('<a href="/novosti/' + link + '.html\">' + title + ' ' + nr + '</a>');
                $('.index_news_region_block').hide();
            }
        }, 'json');
        return false;
    });
});

var regionl1=null;

function mpObjTypeChange()
{
    $(':input','#mpSearchForm')
    .filter(':radio')
    .removeAttr('checked')
    .removeAttr('selected');

    $('#priceClassDefault').attr('checked', 'false');
    $('#priceClassDefault').val('');
    if ($('#type1').val()==1 && $('#type2').val()<=2){
        $('#priceClassDefault').attr('checked', 'true');
        $('#priceClassDefault').val('2');
    }
    $('#type3_1').attr('checked', 'true');

    $('#searchNewbuildLabel').html('От застройщиков');
    if ($('#type2').val()==2 || $('#type2').val()==3){
        $('#searchNewbuildLabel').html('Дома в коттеджных городках');
    }

    $('.shMHide').show();
    if ($('#type1').val()>1)
        $('.shMHide').hide();
   
   if ($('#type1').val()==1 || $('#type1').val()==0){
       $('.shBtnNext').hide();
       $('.shBtnSearch').show();
   }else{
       $('.shBtnNext').show();
       $('.shBtnSearch').hide();
   }
   
    objTypeChange(1);
    mpRegionChange();
}
   
function mpRegionChange()
{
    $('#region2').empty();
    $.post(urlpath+'nedvizhimost/subRegions.ajax', {
        t1: $('#type1').val(), 
        t2: $('#type2').val(), 
        r: $('#region1').val()
    }, 
    function(data)
    {
        $('#region2').append('<option value=""> --- Район --- </option>');
        for (v in data)
        {
            $('#region2').append('<option value="'+v+'">'+data[v]+'</option>');
        }
    },
    'json');
}

function mpSubmitForm(map)
{
    f=$('#mpSearchForm');
    if ($('#type1').val()>1)
    {
        tmMove($('#type1').val(), authorized);
        return;
    }

    link=null;
    tl={
        0:  'kupit-kvartiry',   
        1:'snyat-kvartiru',   
        2:'prodazha-domov',  
        3:'arenda-domov',
        4:  'prodazha-kommercheskoj',   
        5:'arenda-kommercheskoj',   
        6:'prodazha-uchastkov',   
        7:'arenda-uchastkov',
        10: 'prodazha-v-novostrojkax', 
        11: 'arenda-v-novostrojkax',
        20: 'kvartiry-ot-zastrojshhikov', 
        22: 'doma-v-kottedzhnyx-gorodkax', 
        24: 'nezhilye-ot-zastrojshhikov',
        30: 'bank-konfiskat-flat',    
        32: 'bank-konfiskat-house',   
        34: 'bank-konfiskat-office', 
        36: 'bank-konfiskat-land'
    };
    t=parseInt($('#type1').val())+parseInt($('#type2').val())+parseInt(/*$('[name=type3]:checked').val()*/0);

    if (!tl[t]){
        alert('Странно. Такой страницы не существует. :(');
        return;
    }
    if (regionl1[$('#region1').val()]) link=''+regionl1[$('#region1').val()]+'/'+tl[t]+'.html';
    if ($('[name=PRICE_CLASS]:checked').val()==3 && parseInt($('#type2').val())==0)
        link=''+regionl1[$('#region1').val()]+'/arenda-kvartir-posutochno.html';
    if ($('[name=PRICE_CLASS]:checked').val()==3 && parseInt($('#type2').val())==2)
        link=''+regionl1[$('#region1').val()]+'/snyat-dom-posutochno.html';
    
    q=[
        'REGIONS='+getRegion(),
        'oClass='+$('[name=object_class]').val(),
        'codeSearch='+$('[name=codeSearch]').val(),
        'ALL_AREA_FROM='+$('[name=ALL_AREA_FROM]').val(),
        'ALL_AREA_TO='+$('[name=ALL_AREA_TO]').val(),
        'LAND_AREA_FROM='+$('[name=LAND_AREA_FROM]').val(),
        'LAND_AREA_TO='+$('[name=LAND_AREA_TO]').val(),
        'PRICE_FROM='+$('[name=PRICE_FROM]').val(),
        'PRICE_TO='+$('[name=PRICE_TO]').val(),
        'PRICE_CLASS='+$('[name=PRICE_CLASS]:checked').val()
    ];
    if(map == 1)
        map = 'mapSearch=1&';
    else 
        map = '';
    location.href=urlpath+'nedvizhimost/'+link+'?'+ map +q.join('&');
}

function statAjaxUAH(o, i)
{
    if (o) $(o).closest('ul').find('a').removeClass('active');
    if (o) $(o).addClass('active');
    $('#statBlockWrpUAH').load(urlpath+'data/statWidget.html', {
        type: i,
        cur: 'UAH'
    });
}
function statAjaxUSD(o, i)
{
    if (o) $(o).closest('ul').find('a').removeClass('active');
    if (o) $(o).addClass('active');
    $('#statBlockWrpUSD').load(urlpath+'data/statWidget.html', {
        type: i,
        cur: 'USD'
    });
}
   


/* Каруселька */

function carouselInit(carousel, state){

    /*$('#mycarouselIndex').jcarousel({
        itemLoadCallback: mycarousel_itemLoadCallback,
        wrap: 'circular'
    });
    
    $('.jcarousel-prev').attr('title', 'Назад')
    $('.jcarousel-next').attr('title', 'Вперед')*/
    
    
    var jcarousel = $('#index-jcarousel').jcarousel({
        wrap: "circular"
    });

    $('.jcarousel-prev')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '-=3'
        });

    $('.jcarousel-next')
        .on('jcarouselcontrol:active', function() {
            $(this).removeClass('inactive');
        })
        .on('jcarouselcontrol:inactive', function() {
            $(this).addClass('inactive');
        })
        .jcarouselControl({
            target: '+=3'
        });

    var setup = function(data) {
        var html = '<ul>';

        if(data.items) {

            $.each(data.items, function() {
                var youtube = '';
                if(this.youtube) {
                    youtube = '<p class="player-ico"><a target="_blank" rel="nofollow" href="' + this.url + '"></a></p>'; 
                }

                html += '<div class="index-crousel-item-block item-id-'+this.id+' ">\n\
                            <a rel="nofollow" href="'+this.url+'">\n\
                                <img alt="" src="/' + this.main_image.name + '_145x175' + this.main_image.ext + '" />\n\
                            </a>\n\
                            <p class="short_name"><a rel="nofollow" href="'+this.url+'"><b>'+this.short_name+'</b></a></p>\n\
                            <p class="red">Цена: '+this.informer_price+'</p>'+ youtube + '\n\
                        </div>';
            });

            html += '</ul>';

            // Append items
            jcarousel
                .html(html);

            // Reload carousel
            jcarousel
                .jcarousel('reload');
        } else {
            $('.random_baner_index').hide();
        }
    };

    $.getJSON('/index/jcarousel.ajax', setup);
    
    
    //carouselHover();
   
}

function carouselNewbuildInit(){
    $('.informer_elem_slide').jcarousel({
        wrap: 'circular',
        scroll: 1
    });
}

function carouselHover(data){
    $('.jcarousel-item').hover(
        function(){
            $("#divToolTip").remove();
            pos = $(this).position();
            that = $(this);
            
            list = $('.jcarousel-list').position()
            pos.left = pos.left + list.left;
            str = "<div id='divToolTip' class='spanToolTip mainMenuOtherDrop tooltip' style='left:" + (pos.left + 24) + "px; top:" + (pos.top + 11) + "px;' >"+that.find('[innerData=1]').html()+"</div>";

            $('#mycarouselIndex').append(str);
            //$(this).find('.item-hover').show();
        },
        function(){
            $('#mycarouselIndex').find("div#divToolTip").mouseleave(function() { 
                $("#divToolTip").remove(); 
            });
            //$(this).find('.item-hover').hide();
        }
    );
        
        
}


function mycarousel_itemLoadCallback(carousel, state, first, last)
{
    //console.log('Carousel');
    if (carousel.has(carousel.first, carousel.last)) {
        return;
    }
    
    $.get('/index/jcarousel.ajax', 
    {
        first: carousel.first,
        last: carousel.last
    },
    function(data){
        carousel.size(data.total);
        //alert(carousel.first);
        for (var i = 0; i < data.total; i++){
            carousel.add(carousel.first + i, mycarousel_getItemHTML(data[i]))
        }
        //carouselHover(data);
    },'json');
    
};


function indexPriceTab() {
    $('.sale_flat_tabs a').on('click', function(){
        $('.sale_flat_tabs a').toggleClass('activ');
        //$(this).addClass('activ');
        $('.sale_flat_tab').removeClass('activ');
        tabs = $(this).data('href');
        $('#' + tabs).addClass('activ');
        return false;
    })
}
// Хардкод нах 
function mycarousel_getItemHTML(data) {
    var youtube = '';
    if(data.youtube) {
        youtube = '<p class="player-ico"><a target="_blank" rel="nofollow" href="' + data.url + '"></a></p>'; 
    }
    
    /*return '<div class="index-crousel-item-block item-id-'+data.id+' ">\n\
                <div innerData="1" style="display: none;">\n\
                    <a rel="nofollow" href="'+data.url+'">\n\
                        <img alt="" src='+data.image_b+' />\n\
                    <a/>\n\
                    <a href="'+data.url+'">\n\
                        <p class="item-name">'+data.short_desc+'</p>\n\
                    </a>\n\
                    <p class="red">Цена: '+data.informer_price+'</p>\n\
                    <a href="'+data.build_card+'" class="image_build">\n\
                        <img src="'+data.image_build+'" alt="" />\n\
                    </a>\n\
                    <a href="'+data.build_card+'">\n\
                        <p>'+data.company_name+'</p>\n\
                    </a>\n\
                </div>\n\
                <a rel="nofollow" href="'+data.url+'">\n\
                    <img alt="" src="/' + data.main_image.name + '_145x175' + data.main_image.ext + '" />\n\
                </a>\n\
                <p class="short_name"><a rel="nofollow" href="'+data.url+'"><b>'+data.short_name+'</b></a></p>\n\
                <p class="red">Цена: '+data.informer_price+'</p>'+ youtube + '\n\
            </div>';*/
    return '<div class="index-crousel-item-block item-id-'+data.id+' ">\n\
                <a rel="nofollow" href="'+data.url+'">\n\
                    <img alt="" src="/' + data.main_image.name + '_145x175' + data.main_image.ext + '" />\n\
                </a>\n\
                <p class="short_name"><a rel="nofollow" href="'+data.url+'"><b>'+data.short_name+'</b></a></p>\n\
                <p class="red">Цена: '+data.informer_price+'</p>'+ youtube + '\n\
            </div>';
};

/*!
 * jQuery Raty - A Star Rating Plugin - http://wbotelhos.com/raty
 * ---------------------------------------------------------------------
 */

_USE_STAR_RATINGS=1;

;(function($) {

	var methods = {
		init: function(options) {
			return this.each(function() {

            if ($(this).attr('inited')==1)
               return;
            $(this).attr('inited', 1);
            
            // *** Pre-Init Def Options (by Sem)
            $.fn.raty.defaults.path=images+'raty/';
				
            
            var opt		= $.extend({}, $.fn.raty.defaults, options);
            if ($(this).attr('readonly')>0)
               opt.readOnly=true;
            
            opt.start=$(this).attr('ratingval');
   			$this	= $(this).data('options', opt);
            
            if ($(this).attr('header'))
               $this.append('<div class="rateHeader">'+$(this).attr('header')+'<span class="rateHeaderVal"></span></div>');
            
				if (opt.number > 20) {
					opt.number = 20;
				} else if (opt.number < 0) {
					opt.number = 0;
				}

				if (opt.round.down === undefined) {
					opt.round.down = $.fn.raty.defaults.round.down;
				}

				if (opt.round.full === undefined) {
					opt.round.full = $.fn.raty.defaults.round.full;
				}

				if (opt.round.up === undefined) {
					opt.round.up = $.fn.raty.defaults.round.up;
				}

				if (opt.path.substring(opt.path.length - 1, opt.path.length) != '/') {
					opt.path += '/';
				}

				if (typeof opt.start == 'function') {
					opt.start = opt.start.call(this);
				}

				var isValidStart	= !isNaN(parseInt(opt.start, 10)),
					start			= '';

				if (isValidStart) {
					start = (opt.start > opt.number) ? opt.number : opt.start;
				} 

				var starFile	= opt.starOn,
					space		= (opt.space) ? 4 : 0,
					hint		= '';

				for (var i = 1; i <= opt.number; i++) {
					starFile = (start < i) ? opt.starOff : opt.starOn;

					hint = (i <= opt.hintList.length && opt.hintList[i - 1] !== null) ? opt.hintList[i - 1] : i;

					$this.append('<img src="' + opt.path + starFile + '" alt="' + i + '" title="' + hint + '" />');

					if (opt.space) {
						$this.append((i < opt.number) ? '&nbsp;' : '');
					}
				}

            $this.append('<div class="rateVotesCount">Голосов: <span class="rateHeaderVotes"></span></div>');
            $this.append('<div class="rateVoteComplete">Спасибо, Ваш голос учтён</div>');

				var $score = $('<input/>', { type: 'hidden', name: opt.scoreName}).appendTo($this);

				if (isValidStart) {
					if (opt.start > 0) {
						$score.val(start);
					}

					methods.roundStar.call($this, start);
				}

				if (opt.iconRange) {
					methods.fillStar.call($this, start);	
				}

				methods.setTarget.call($this, start, opt.targetKeep);
            methods.setHeaders.call($this);

				var width = opt.width || (opt.number * opt.size + opt.number * space);

				if (opt.cancel) {
					var $cancel = $('<img src="' + opt.path + opt.cancelOff + '" alt="x" title="' + opt.cancelHint + '" class="raty-cancel"/>');

					if (opt.cancelPlace == 'left') {
						$this.prepend('&nbsp;').prepend($cancel);
					} else {
						$this.append('&nbsp;').append($cancel);
					}

					width += opt.size + space;
				}

				if (opt.readOnly) {
					methods.fixHint.call($this);

					$this.children('.raty-cancel').hide();
				} else {
					$this.find('img').not('.raty-cancel').css('cursor', 'pointer');

					methods.bindAction.call($this);
				}

				//$this.css('width', width);
			});
		}, bindAction: function() {
			var self	= this,
				opt		= this.data('options'),
				$score	= this.children('input');

			self.mouseleave(function() {
				methods.initialize.call(self, $score.val());

				methods.setTarget.call(self, $score.val(), opt.targetKeep);
			});

			var $stars	= this.children('img').not('.raty-cancel'),
				action	= (opt.half) ? 'mousemove' : 'mouseover';

			if (opt.cancel) {
				self.children('.raty-cancel').mouseenter(function() {
					$(this).attr('src', opt.path + opt.cancelOn);

					$stars.attr('src', opt.path + opt.starOff);

					methods.setTarget.call(self, null, true);
				}).mouseleave(function() {
					$(this).attr('src', opt.path + opt.cancelOff);

					self.mouseout();
				}).click(function(evt) {
					$score.removeAttr('value');

					if (opt.click) {
			          opt.click.call(self[0], null, evt);
			        }
				});
			}

			$stars.bind(action, function(evt) {
				var value = parseInt(this.alt, 10);

				if (opt.half) {
					var position	= parseFloat((evt.pageX - $(this).offset().left) / opt.size),
						diff		= (position > .5) ? 1 : .5;

					value = parseFloat(this.alt) - 1 + diff;

					methods.fillStar.call(self, value);

					if (opt.precision) {
						value = value - diff + position;
					}

					methods.showHalf.call(self, value);
				} else {
					methods.fillStar.call(self, value);
				}

				self.data('score', value);

				methods.setTarget.call(self, value, true);
			}).click(function(evt) {
            
            // *** Send Vote (By Sem)
            nVal=((opt.half || opt.precision) ? self.data('score') : this.alt);
            if (confirm('Моя оценка - '+(nVal*2)+' из 10 баллов.'))
               {
               $score.val(nVal);
               $.get(urlpath+'data/setRating.json', {'type': $(self[0]).attr('starrating'), 'id': $(self[0]).attr('ratingid'), 'val': $score.val()},
                     function(data)
                        {  
                        $(self[0]).find('.rateVoteComplete').html(data.status).show().fadeOut(1500);
                        methods.readOnly.call($(self[0]), true);
                        if (data.rating>0)
                           {
                           $(self[0]).attr('ratingval', data.rating);
                           methods.initialize.call($(self[0]), data.rating);
                           }
                        if (data.votes_count>0){$(self[0]).attr('votes_count', data.votes_count);}
                        methods.setHeaders.call($(self[0]));
                        }
                     , 'json');
               if (opt.click) {opt.click.call(self[0], $score.val(), evt);}
               }            

			});
		}, cancel: function(isClick) {
			return this.each(function() {
				var $this = $(this);

				if ($this.data('readonly') == 'readonly') {
					return false;
				}

				if (isClick) {
					methods.click.call($this, null);
				} else {
					methods.start.call($this, null);
				}

				$this.mouseleave().children('input').removeAttr('value');
			});
		}, click: function(score) {
			return this.each(function() {
				var $this = $(this);

            var opt = $this.data('options');

				if ($this.data('readonly') == 'readonly') {
					return false;
				}
            
				methods.initialize.call($this, score);
            
				if (opt.click) {
					opt.click.call($this[0], score);
				} else {
					$.error('you must add the "click: function(score, evt) { }" callback.');
				}

				methods.setTarget.call($this, score, true);
			});
		}, fillStar: function(score) {
			var opt		= this.data('options'),
				$stars	= this.children('img').not('.raty-cancel'),
				qtyStar	= $stars.length,
				count	= 0,
				$star	,
				star	,
				icon	;

			for (var i = 1; i <= qtyStar; i++) {
				$star = $stars.eq(i - 1);

				if (opt.iconRange && opt.iconRange.length > count) {
					star = opt.iconRange[count];

					if (opt.single) {
						icon = (i == score) ? (star.on || opt.starOn) : (star.off || opt.starOff);
					} else {
						icon = (i <= score) ? (star.on || opt.starOn) : (star.off || opt.starOff);
					}

					if (i <= star.range) {
						$star.attr('src', opt.path + icon);
					}

					if (i == star.range) {
						count++;
					}
				} else {
					if (opt.single) {
						icon = (i == score) ? opt.starOn : opt.starOff;
					} else {
						icon = (i <= score) ? opt.starOn : opt.starOff;
					}

					$star.attr('src', opt.path + icon);
				}
			}
		}, fixHint: function() {
			var opt		= this.data('options'),
				$score	= this.children('input'),
				score	= parseInt($score.val(), 10),
				hint	= opt.noRatedMsg;

			if (!isNaN(score) && score > 0) {
				hint = (score <= opt.hintList.length && opt.hintList[score - 1] !== null) ? opt.hintList[score - 1] : score;
			}

			$score.attr('readonly', 'readonly');
			this.css('cursor', 'default').data('readonly', 'readonly').attr('title', hint).children('img').attr('title', hint);
         $(this).find('img').not('.raty-cancel').css('cursor', 'default');
		}, readOnly: function(isReadOnly) {
			return this.each(function() {
				var $this	= $(this),
					$cancel	= $this.children('.raty-cancel');

				if ($cancel.length) {
					if (isReadOnly) {
						$cancel.hide();
					} else {
						$cancel.show();
					}
				}

				if (isReadOnly) {
					$this.unbind();

					$this.children('img').unbind();

					methods.fixHint.call($this);
				} else {
					methods.bindAction.call($this);

					methods.unfixHint.call($this);
				}
			});
		}, roundStar: function(score) {
			var opt		= this.data('options'),
				diff	= (score - Math.floor(score)).toFixed(2);

			if (diff > opt.round.down) {
				var icon = opt.starOn;						// Full up: [x.76 .. x.99]

				if (diff < opt.round.up && opt.halfShow) {	// Half: [x.26 .. x.75]
					icon = opt.starHalf;
				} else if (diff < opt.round.full) {			// Full down: [x.00 .. x.5]
					icon = opt.starOff;
				}

				this.children('img').not('.raty-cancel').eq(Math.ceil(score) - 1).attr('src', opt.path + icon);
			}												// Full down: [x.00 .. x.25]
		}, score: function() {
			var score	= [],
				value	;

			this.each(function() {
				value = $(this).children('input').val();
				value = (value == '') ? null : parseFloat(value);

				score.push(value);
			});

			return (score.length > 1) ? score : score[0];
		}, setHeaders: function() {
         this.each(function() {

            $this=$(this);
            rating=$this.attr('ratingval');
            votes_count=parseInt($this.attr('votes_count'));

            $this.find('.rateHeaderVal').html(rating>0 ? (rating*2)+' / 10' : '-');
            $this.find('.rateHeaderVotes').html(votes_count>0 ? votes_count : 0);
         
         });
      }, setTarget: function(value, isKeep) {
			var opt = this.data('options');

			if (opt.target) {
				var $target = $(opt.target);

				if ($target.length == 0) {
					$.error('target selector invalid or missing!');
				} else {
					var score = value;

					if (score == null && !opt.cancel) {
						$.error('you must enable the "cancel" option to set hint on target.');
					} else {
						if (!isKeep || score == '') {
							score = opt.targetText;
						} else {
							if (opt.targetType == 'hint') {
								if (score === null && opt.cancel) {
									score = opt.cancelHint;
								} else {
									score = opt.hintList[Math.ceil(score - 1)];
								}
							} else {
								if (score != '' && !opt.precision) {
									score = parseInt(score, 10);
								} else {
									score = parseFloat(score).toFixed(1);
								}
							}
						}

						if (opt.targetFormat.indexOf('{score}') < 0) {
							$.error('template "{score}" missing!');
						} else if (value !== null) {
							score = opt.targetFormat.toString().replace('{score}', score);
						}

						if ($target.is(':input')) {
							$target.val(score);
						} else {
							$target.html(score);
						}
					}
				}
			}
		}, showHalf: function(score) {
			var opt		= this.data('options'),
				diff	= (score - Math.floor(score)).toFixed(1);

			if (diff > 0 && diff < .6) {
				this.children('img').not('.raty-cancel').eq(Math.ceil(score) - 1).attr('src', opt.path + opt.starHalf);
			}
		}, start: function(score) {
			return this.each(function() {
				var $this = $(this);

				if ($this.data('readonly') == 'readonly') {
					return false;
				}

				methods.initialize.call($this, score);

				var opt = $this.data('options');

				methods.setTarget.call($this, score, true);
			});
		}, initialize: function(score) {
			var opt	= this.data('options');
         
			if (score < 0) {
				score = 0;
			} else if (score > opt.number) {
				score = opt.number;
			}

			methods.fillStar.call(this, score);

			if (score != '') {
				if (opt.halfShow) {
					methods.roundStar.call(this, score);
				}

				this.children('input').val(score);
			}
		}, unfixHint: function() {
			var opt		= this.data('options'),
				$imgs	= this.children('img').filter(':not(.raty-cancel)');

			for (var i = 0; i < opt.number; i++) {
				$imgs.eq(i).attr('title', (i < opt.hintList.length && opt.hintList[i] !== null) ? opt.hintList[i] : i);
			}

			this.removeData('readonly').removeAttr('title').children('input').attr('readonly', 'readonly');
         $(this).find('img').not('.raty-cancel').css('cursor', 'pointer');
		}
	};

	$.fn.raty = function(method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method ' + method + ' does not exist!');
		} 
	};

	$.fn.raty.defaults = {
		cancel:			false,
		cancelHint:		'cancel this rating!',
		cancelOff:		'cancel-off.png',
		cancelOn:		'cancel-on.png',
		cancelPlace:	'left',
		click:			undefined,
		half:			   true,
		halfShow:		true,
		hintList:		['Очень плохо', 'Плохо', 'Нормально', 'Хорошо', 'Отлично'],
		iconRange:		undefined,
		noRatedMsg:		'not rated yet',
		number:			5,
		path:			   'raty/',
		precision:		false,
		round:			{ down: .25, full: .5, up: .75 },
		readOnly:		false,
		scoreName:		'score',
		single:			false,
		size:			   16,
		space:			true,
		starHalf:		'star-half.png',
		starOff:		'star-off.png',
		starOn:			'star-on.png',
		start:			0,
		target:			undefined,
		targetFormat:	'{score}',
		targetKeep:		false,
		targetText:		'',
		targetType:		'hint',
		width:			undefined
	};

})(jQuery);
(function($){
   $(document).bind("click touchstart", function(event){
      if ($(event.target).closest('.treecombo-ulWrapper').length) return;
      else if ($(event.target).closest('.treecombo').length)
         $(event.target).closest('.treecombo').treecombo("toggle");
      else $('.treecombo-ulWrapper').hide();
      event.stopPropagation();
      });
   
   $.fn.treecombo=function(options)
   {
   var settings={
      width: '150px',
      comboHeight: '200px',
      placeholder: '',
      treeControl: true
      };      

   function buildDescription(obj, txt)
      {
      dsc=[];
      $.each( obj.find('.jquery-tree-checked'), function(key, val)
         {
         dsc.push($($(val).closest('label')).text());
         });

      // *** Грёбанные ослы не поддерживают indexOf. Ну и хер с ними - не будем заморачиваться.
      if ([].indexOf)
         {
         $.each( obj.find('.jquery-tree-checked'), function(key, val)
            {
            if ($(val).closest('li').find('ul').find('.jquery-tree-checked').length>0 && $(val).closest('li').find('ul').find('.jquery-tree-unchecked').length==0 )
               {
               $.each( $(val).closest('li').find('ul').find('.jquery-tree-checked'), function(key1, val1){
                  d=dsc.indexOf($(val1).text());
                  if (d>-1)
                     dsc.splice(d, 1);
                  });
               } 
            });
         }

      txt.html(dsc.join(', '));
      if (txt.html()=='') txt.html('<span class="placeholder">'+txt.closest('[type=treecombo]').data("treecombo").settings.placeholder+'</span>');
      }

   
   var methods = 
      {
      init: function(options)
         {
         if (options) $.extend(settings, options);
         
         var $this=$(this);
         $this.addClass('treecombo');
         $this.append('<div class=txt></div>');
         $this.css({width: settings.width});
         $this.attr('type', 'treecombo');
         
         if ($this.attr('placeholder'))
            settings.placeholder=$this.attr('placeholder');
         
         $this.find('ul').first().show().wrap('<div class="treecombo-ulWrapper" />');
         $this.find('ul').first().Tree({noControl: !settings.treeControl});
         var ulWrapper=$this.find('.treecombo-ulWrapper');
         $this.data('treecombo', {'ulWrapper': ulWrapper, 'settings': settings});
         ulWrapper.insertAfter($this);
         
         settings.comboWidth=settings.comboWidth ? settings.comboWidth : settings.width;
         ulWrapper.css({width: settings.comboWidth, height: settings.comboHeight});
         
         ulWrapper.find('input[type=checkbox]').bind("change", function()
            {
            buildDescription(ulWrapper, $this.find('.txt')) 
            });
         
         // *** Костыль для оперы (как это ни удивительно, но там не работает событие change!!!! О_о)
         if (jQuery.browser.opera)
            {
            ulWrapper.find('input[type=checkbox]').bind("click", function()
               {
               setTimeout(function(){buildDescription(ulWrapper, $this.find('.txt'));}, 50); 
               });
            }
         
         buildDescription(ulWrapper, $this.find('.txt'));
         },
      toggle: function()
         {
         w=$(this).data("treecombo").ulWrapper;
         lastState=w.css('display');
         w.css({ left: $(this).position().left+2, top: $(this).position().top+$(this).height()+1, 'z-index': 999999 });
         
         $('.treecombo-ulWrapper').hide();
         if (lastState=='none')
            w.show();
         },
      clear: function()
         {
         w=$(this).data("treecombo").ulWrapper;
         w.find(':checked').removeProp('checked');
         w.find('.jquery-tree-checked, .jquery-tree-checked-partial').
            removeClass('jquery-tree-checked').
            removeClass('jquery-tree-checked-partial').
            addClass('jquery-tree-unchecked');
         buildDescription(w, $(this).find('.txt'));
         }
      }
   
   var cArg=arguments;
   
   this.each(function(k, v)
      {
      if ( methods[options] )
         {
         return methods[ options ].apply( v, Array.prototype.slice.call( cArg, 1 ));
         } 
      else if ( typeof options === 'object' || ! options ) 
         {
         return methods.init.apply( v, cArg );
         }
      });
   }

   // *** Tree Checkbox Class
   var CLASS_JQUERY_TREE = 'jquery-tree';
   var CLASS_JQUERY_TREE_CONTROLS = 'jquery-tree-controls';
   var CLASS_JQUERY_TREE_COLLAPSE_ALL = 'jquery-tree-collapseall';
   var CLASS_JQUERY_TREE_EXPAND_ALL = 'jquery-tree-expandall';
   var CLASS_JQUERY_TREE_COLLAPSED = 'jquery-tree-collapsed';
   var CLASS_JQUERY_TREE_HANDLE = 'jquery-tree-handle';
   var CLASS_JQUERY_TREE_TITLE = 'jquery-tree-title';
   var CLASS_JQUERY_TREE_NODE = 'jquery-tree-node';
   var CLASS_JQUERY_TREE_LEAF = 'jquery-tree-leaf';
   var CLASS_JQUERY_TREE_CHECKED = 'jquery-tree-checked';
   var CLASS_JQUERY_TREE_UNCHECKED = 'jquery-tree-unchecked';
   var CLASS_JQUERY_TREE_CHECKED_PARTIAL = 'jquery-tree-checked-partial';

   var COLLAPSE_ALL_CODE = '<span class="' + CLASS_JQUERY_TREE_COLLAPSE_ALL + '">Свернуть все</span>';
   var EXPAND_ALL_CODE = '<span class="' + CLASS_JQUERY_TREE_EXPAND_ALL + '">Развернуть все</span>';
   var TREE_CONTROLS_CODE = '<div class="' + CLASS_JQUERY_TREE_CONTROLS + '">' +
   COLLAPSE_ALL_CODE +
   EXPAND_ALL_CODE +
   '</div>';

   var TREE_NODE_HANDLE_CODE = '<span class="' + CLASS_JQUERY_TREE_HANDLE + '">+</span>';
   var TREE_NODE_HANDLE_EMPTY_CODE = '<span class="' + CLASS_JQUERY_TREE_HANDLE + '">&nbsp;</span>';
   var TREE_NODE_HANDLE_COLLAPSED = "+";
   var TREE_NODE_HANDLE_EXPANDED = "&minus;";

   $.fn.extend({

      /**
       * Делает дерево из структуры вида:
       * <ul>
       *   <li><label><input type="checkbox" />Item1</label></li>
       *   <li>
       *     <label><input type="checkbox" />ItemWithSubitems</label>
       *     <ul>
       *       <li><label><input type="checkbox" />Subitem1</label></li>
       *     </ul>
       *   </li>
       * </ul>
       */
      Tree: function(opt){
         // Добавим контролы для всего дерева (все свернуть, развернуть и т.д.), и добавим класс
         if (!opt)opt={};
         cc=TREE_CONTROLS_CODE;
         if (opt.noControl)
            cc=null;
            
         $(this)
            .addClass(CLASS_JQUERY_TREE)
            .before(cc)
            .prev('.' + CLASS_JQUERY_TREE_CONTROLS)
            .find('.' + CLASS_JQUERY_TREE_COLLAPSE_ALL).click(function(){
               $(this).parent().next('.' + CLASS_JQUERY_TREE)
                  .find('li:has(ul)')
                  .addClass(CLASS_JQUERY_TREE_COLLAPSED)
                  .find('.' + CLASS_JQUERY_TREE_HANDLE)
                  .html(TREE_NODE_HANDLE_COLLAPSED);
            })

            .parent('.' + CLASS_JQUERY_TREE_CONTROLS).find('.' + CLASS_JQUERY_TREE_EXPAND_ALL)
               .click(function(){
                  $(this).parent().next('.' + CLASS_JQUERY_TREE)
                     .find('li:has(ul)')
                        .removeClass(CLASS_JQUERY_TREE_COLLAPSED)
                     .find('.' + CLASS_JQUERY_TREE_HANDLE)
                        .html(TREE_NODE_HANDLE_EXPANDED);
               });

         $('li', this).find(':first').addClass(CLASS_JQUERY_TREE_TITLE)
            .closest('li').addClass(CLASS_JQUERY_TREE_LEAF);
          
         //$('li:not(:has(ul))', this).find(':first').before(TREE_NODE_HANDLE_EMPTY_CODE);
         // Для всех элементов, являющихся узлами (имеющих дочерние элементы)...
         $('li:has(ul:has(li))', this).find(':first')
            // ... добавим элемент, открывающий/закрывающий узел
            .before(TREE_NODE_HANDLE_CODE)
            // ... добавим к контейнеру класс "узел дерева" и "свернем".
            .closest('li')
               .addClass(CLASS_JQUERY_TREE_NODE)
               .addClass(CLASS_JQUERY_TREE_COLLAPSED)
               .removeClass(CLASS_JQUERY_TREE_LEAF);

         // ... повесим обработчик клика
         $('.' + CLASS_JQUERY_TREE_HANDLE, this).bind('click', function(){
            var leafContainer = $(this).parent('li');
            var leafHandle = leafContainer.find('>.' + CLASS_JQUERY_TREE_HANDLE);

            leafContainer.toggleClass(CLASS_JQUERY_TREE_COLLAPSED);

            if (leafContainer.hasClass(CLASS_JQUERY_TREE_COLLAPSED))
               leafHandle.html(TREE_NODE_HANDLE_COLLAPSED);
            else
               leafHandle.html(TREE_NODE_HANDLE_EXPANDED);
         });

         // Добавляем обработку клика по чекбоксам
         $('input:checkbox', this).click(function(){
            setLabelClass(this);
            checkCheckbox(this);
         })
         // Выставляем чекбоксам изначальные классы
         .each(function(){
            setLabelClass(this);
            if (this.checked)
               checkParentCheckboxes(this);
         })
         // Для IE вешаем обработчики на лейбл
         .closest('label').click(function(){
            labelClick(this);
            checkCheckbox($('input:checkbox', this));
         });
      }

   });

   /**
    * Рекурсивно проверяет, все ли чекбоксы в поддереве родительского узла выбраны.
    * Если ни один чекбокс не выбран - снимает чек с родительского чекбокса
    * Если хотя бы один, но не все - выставляет класс CLASS_JQUERY_TREE_CHECKED_PARTIAL родительскому чекбоксу
    * Если все - ставит чек на родительский чекбокс
    *
    * @param {Object} checkboxElement текущий чекбокс
    */
   function checkParentCheckboxes(checkboxElement){
      if (typeof checkboxElement == 'undefined' || !checkboxElement)
         return;

      // проверим, все ли чекбоксы выделены/частично выделены на вышележащем уровне
      var closestNode = $(checkboxElement).closest('ul');
      var allCheckboxes = closestNode.find('input:checkbox');
      var checkedCheckboxes = closestNode.find('input:checkbox:checked');

      var allChecked = allCheckboxes.length == checkedCheckboxes.length;

      var parentCheckbox = closestNode.closest('li').find('>.' + CLASS_JQUERY_TREE_TITLE + ' input:checkbox');

      if (parentCheckbox.length > 0) {
         parentCheckbox.get(0).checked = allChecked;

         if (!allChecked && checkedCheckboxes.length > 0)
            parentCheckbox.closest('label')
               .addClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL)
               .removeClass(CLASS_JQUERY_TREE_CHECKED)
               .removeClass(CLASS_JQUERY_TREE_UNCHECKED);
         else
            if (allChecked)
               parentCheckbox.closest('label')
                  .removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL)
                  .removeClass(CLASS_JQUERY_TREE_UNCHECKED)
                  .addClass(CLASS_JQUERY_TREE_CHECKED);
            else
               parentCheckbox.closest('label')
                  .removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL)
                  .removeClass(CLASS_JQUERY_TREE_CHECKED)
                  .addClass(CLASS_JQUERY_TREE_UNCHECKED);

         checkParentCheckboxes(parentCheckbox.get(0));
      }
   }

   /**
    * Если у текущего чекбокса есть дочерние узлы - меняет их состояние
    * на состояние текущего чекбокса
    *
    * @param {Object} checkboxElement текущий чекбокс
    */
   function checkCheckbox(checkboxElement){
      // чекнем/анчекнем нижележащие чекбоксы
      $(checkboxElement).closest('li').find('input:checkbox').each(function(){
         this.checked = $(checkboxElement).prop('checked');
         setLabelClass(this);
      });
      checkParentCheckboxes(checkboxElement);
   };

   /**
    * Выставляет класс лейблу в зависимости от состояния чекбокса
    *
    * @param {Object} checkboxElement чекбокс
    */
   function setLabelClass(checkboxElement){
      isChecked = $(checkboxElement).prop('checked');

      if (isChecked) {
         $(checkboxElement).closest('label')
            .addClass(CLASS_JQUERY_TREE_CHECKED)
            .removeClass(CLASS_JQUERY_TREE_UNCHECKED)
            .removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL);
      }
      else {
         $(checkboxElement).closest('label')
            .addClass(CLASS_JQUERY_TREE_UNCHECKED)
            .removeClass(CLASS_JQUERY_TREE_CHECKED)
            .removeClass(CLASS_JQUERY_TREE_CHECKED_PARTIAL);
      }
   };

   /**
    * Обрабатывает клик по лейблу (для IE6)
    */
   function labelClick(labelElement){
      var checkbox = $('input:checkbox', labelElement);
      var checked = checkbox.prop('checked');
      checkbox.prop('checked', !checked);
      setLabelClass(checkbox);
   } 
   
  
})(jQuery);
var realtTypeChange = false;
function bigSearchFormInit()
{
    $('#regionSelect').treecombo({
        width: '100%',
        comboWidth: '220px',
        comboHeight: '350px',
        'placeholder': 'Регион',
        'treeControl': false
    });

    $('#regionSelectNb').treecombo({
        width: '100%',
        comboWidth: '220px',
        comboHeight: '350px',
        'placeholder': 'Регион',
        'treeControl': false
    });
    $('#regionSelectNt').treecombo({
        width: '100%',
        comboWidth: '270px',
        comboHeight: '350px',
        'placeholder': 'Регион',
        'treeControl': false
    });

    $('#oClass').treecombo({
        width: '100%',
        comboWidth: '220px',
        comboHeight: '350px',
        'placeholder': 'Комнат',
        'treeControl': false
    });
    $('#mitroSelect').treecombo({
        width: '100%',
        comboWidth: '250px',
        comboHeight: '370px',
        'treeControl': false
    });
    $('#mitroSelectNt').treecombo({
        width: '100%',
        comboWidth: '250px',
        comboHeight: '370px',
        'treeControl': false
    });

    $('#wallSelect').treecombo({
        width: '100%',
        comboWidth: '250px',
        comboHeight: '370px',
        'treeControl': false
    });
    
    $('#seriesSelect').treecombo({
        width: '100%',
        comboWidth: '250px',
        comboHeight: '370px',
        'treeControl': false
    });
    $('#seriesSelectNd').treecombo({
        width: '100%',
        'comboWidth': '250px',
        placeholder: 'Классификация',
        'treeControl': false
    });
    $('#seriesSelectNt').treecombo({
        width: '100%',
        comboWidth: '250px',
        comboHeight: '370px',
        'treeControl': false
    });
    $('#newbuildClassSelect').treecombo({
        width: '100%',
        comboWidth: '220px',
        comboHeight: '150px',
        'placeholder': 'Класс',
        'treeControl': false
    });
    $('#readinessSelect').treecombo({
        width: '100%',
        comboWidth: '220px',
        comboHeight: '50px',
        'placeholder': 'Готовность',
        'treeControl': false
    });


    $('[name=streetSearch]').autocomplete(urlpath + 'data/streetsearch_r.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        formatItem: formatStreet,
        max: 100,
        scroll: true,
        extraParams: {'k': regionId},
        onItemSelect: function() {
            $('#houseSearch, [name="HOUSE_NUM"]').val('');
            $('#houseSearch, [name="HOUSE_NUM"]').flushCache();
            //checkStreet();
        }
    });
    $('[name=street]').autocomplete(urlpath + 'data/streetsearch_r.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        formatItem: formatStreet,
        max: 100,
        scroll: true,
        extraParams: {'k': regionId},
        onItemSelect: function() {
            $('#houseSearch, [name="HOUSE_NUM"]').val('');
            $('#houseSearch, [name="HOUSE_NUM"]').flushCache();
            //checkStreet();
        }
    });

    $('[name=HOUSE_NUM]').autocomplete(urlpath + 'data/housesearch.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        matchContains: true,
        matchSubset: false,
        selectFirst: true,
        delay: 30,
        max: 400,
        minChars: 0,
        scroll: true,
        extraParams: {
            'k': regionId,
            's': function() {
                return $('[name=streetSearch]').val();
            },
            't': function() {
                return ($('[name=search_type]').find('option:selected').val());
            },
            'o': function() {
                var i = 0;
                var data = new Array();
                $('.jquery-tree-checked').find('input').each(function() {
                    data[i++] = ($(this).val());
                });
                return (data);
            }
        }
    });


    /*$('[name=streetSearchPhoto]').autocomplete(urlpath + 'data/streetsearch_r.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        formatItem: formatStreet,
        max: 100,
        scroll: true,
        extraParams: {'k': regionId},
        onItemSelect: function() {
            $('#houseSearch, [name="HOUSE_NUM"]').val('');
            $('#houseSearch, [name="HOUSE_NUM"]').flushCache();
            //checkStreet();
        }
    });

    $('[name=HOUSE_NUM_PHOTO]').autocomplete(urlpath + 'data/housesearch.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        matchContains: true,
        matchSubset: false,
        selectFirst: true,
        delay: 30,
        max: 400,
        scroll: true,
        extraParams: {
            'k': regionId,
            's': function() {
                return $('[name=streetSearchPhoto]').val();
            },
            't': function() {
                return ($('[name=search_type]').find('option:selected').val());
            },
            'o': function() {
                var i = 0;
                var data = new Array();
                $('.jquery-tree-checked').find('input').each(function() {
                    data[i++] = ($(this).val());
                });
                return (data);
            }
        }
    });*/
    
    $('[name=streetSearchPhoto]').autocomplete(urlpath + 'data/streetsearch.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        matchContains: false,
        matchSubset: false,
        delay: 30,
        max: 100,
        scroll: true,
        extraParams: {
            'regions': function(){
                r=[];
                $.each( $('[name="regions[]"]:checked'), function(k,v){
                    r.push( $(v).val() );
                });
                return r.join(',');
            },
            'withPhoto': 1
        },
        onItemSelect: function(){$('[name="HOUSE_NUM_PHOTO"]').val('');$('[name="HOUSE_NUM_PHOTO"]').flushCache();checkStreet();}
    });

    $('[name=HOUSE_NUM_PHOTO]').autocomplete(urlpath + 'data/housesearch.json', {
        multiple: false,
        mustMatch: false,
        autoFill: false,
        matchContains: true,
        matchSubset: false,
        selectFirst: true,
       	minChars: 0,
        delay: 30,
        max: 600,
        scroll: true,
        extraParams: {
            'k': function(){
                r=[];
                $.each( $('[name="regions[]"]:checked'), function(k,v){
                    r.push( $(v).val() );
                });
                if(r.lenght > 0)
                    return r.join(',') + ',' + regionId;
                else
                    return regionId;
            },
            's': function(){
                return $('[name=streetSearchPhoto]').val();
            },
            'withPhoto': 1
        }
    });
    
    $('[name=HOUSE_NUM_PHOTO], [name=HOUSE_NUM]').on('mouseenter', function(){
        $(this).click();
    });

    // *** Ебаные костыли
    if(!form_search_type){
        form_search_type = $.urlParam('search_type');
    }

    if(!form_search_type || form_search_type === 'novostrojki'){
        form_search_type = 'kupit-kvartiry';
    }

    $('[name=search_type] [value="' + form_search_type + '"]').attr('selected', 'selected');
    
    $('[name="search_type"]').change(function() {
        searchTypeChange(1);
    });

    $('.index-search-submit').on('click', function() {
        
        url_region = $('.activ [name=url_region]').val();
        url_type = $('.activ [name=search_type]').val();

        /*
         
         var region1 = $('#region1').val();
         
         m = [];
         mit = true;
         if (tl === 'prodazha-domov')
         mit = false;
         if (tl === 'prodazha-uchastkov')
         mit = false;
         if (region1 !== '8000000000')
         mit = false;
         if ($('[name=MITRO]').val() > 0)
         mit = true;
         
         if (mit) {
         m = [
         '&MITRO[]=' + $('[name=MITRO]').val(),
         'MITRO_DISTANCE=' + $('[name=MITRO_DISTANCE]').val(),
         ];
         }*/

        //console.log(opc);
        /*if (map == 1)
         map = 'mapSearch=1&';
         else
         map = '';
         */
        
        //$('.activ #searchRequestForm')[0].reset();
        url_param = $('.activ #searchRequestForm').serialize();
        
        /*var regionsArray = [];
        $(".activ form input[type=checkbox]:checked").each(function(i) {
            regionsArray[i] = this.value;
        });

        q = [
            'lim=' + $('[name=lim]').val(),
            'order=' + $('[name=order]').val(),
            'seq=' + $('[name=seq]').val(),
            'realtType=' + $('[name=realtType]').val(),
            'extSrch=' + $('[name=extSrch]').val(),
            'url_region=' + $('[name=url_region]').val(),
            'REGIONS[]=' + regionsArray.join('&REGIONS[]='),
            'search_type=' + $('[name=search_type]').val(),
            'codeSearch=' + $('[name=codeSearch]').val(),
            'PRICE_FROM=' + $('[name=PRICE_FROM]').val(),
            'PRICE_TO=' + $('[name=PRICE_TO]').val(),
            'PRICE_CLASS=' + $('[name=PRICE_CLASS]').val(),
            'DAYS_AGO=' + $('[name=DAYS_AGO]').val(),
            'MITRO_DISTANCE=' + $('[name=MITRO_DISTANCE]').val(),
            'streetSearch=' + $('[name=streetSearch]').val(),
            'HOUSE_NUM=' + $('[name=HOUSE_NUM]').val(),
            'WALL_TYPE=' + $('[name=WALL_TYPE]').val(),
            'STOREY_FROM=' + $('[name=STOREY_FROM]').val(),
            'STOREY_TO=' + $('[name=STOREY_TO]').val(),
            'ALL_AREA_FROM=' + $('[name=ALL_AREA_FROM]').val(),
            'ALL_AREA_TO=' + $('[name=ALL_AREA_TO]').val(),
            'LIV_AREA_FROM=' + $('[name=LIV_AREA_FROM]').val(),
            'KIT_AREA_FROM=' + $('[name=KIT_AREA_FROM]').val(),
            'LAND_AREA_FROM=' + $('[name=LAND_AREA_FROM]').val(),
            'LAND_AREA_TO=' + $('[name=LAND_AREA_TO]').val(), 
        ];

        url_param =  q.join('&');*/
        
       
        if ($('.activ #searchRequestForm').attr('action'))
            action = $('.activ #searchRequestForm').attr('action');
        else
            action = '/nedvizhimost/' + url_region + '/' + url_type + '.html';
        //href = '/nedvizhimost/' + url_region + '/' + url_type + '.html?' + q.join('&') + m.join('&');
        href = action + '?' + url_param;
        //console.log(href);
        //alert(url_param);
        location.href = href;
        return false;
    });


    $('.index-photo-submit').on('click', function() {
        url_region = $('.activ [name=url_region]').val();
        //action = '/poleznoe/photoalbum-' + url_region + '.html?lim=&order=&seq=&realtType=&extSrch=&url_region=' + url_region + '&street=' + $('[name=streetSearchPhoto]').val() + '&house_num=' + $('[name=HOUSE_NUM_PHOTO]').val() + '';
        
        url_param = $('.activ #searchRequestForm').serialize();
        action = '/poleznoe/photoalbum-' + url_region + '.html?' + url_param +'&' + url_region + '&street=' + $('[name=streetSearchPhoto]').val() + '&house_num=' + $('[name=HOUSE_NUM_PHOTO]').val() + '';
        
        href = action;
        //console.log(href);
        location.href = href;
        return false;
    });

    formTypeChange();

    $('[name="oClass[]"]').click(function() {
        clearAddress();
    });
    regionWidth();
    searchTypeChange(1);
    indexSearchTab();
    
    if (typeof sessionStorage) {
        ct = sessionStorage.getItem('curTab');
        if (ct) {
            //$('a.' + ct).click();
            $('a.' + ct).addClass('activ');
        }
            
    }
    
    $('.streets-click-video').on('click', function(){
        $('.streets-video ul').hide();
        //$(this).closest('li').find('ul').show("slow");
        $(this).closest('li').find('ul').empty();
        streetId = $(this).data('streetid');
        var list = $(this);
        $.post('/poleznoe/getStreetsVideo.json', {streetId: streetId}, function(data){
            for(var i in data.list){
                list.closest('li').find('ul').append('<li><a rel="nofollow" title="Ссылка на видео" target="_blank" href="https://www.youtube.com/watch?v=' + data.list[i].video + '">' + data.list[i].address + '</a></li>');
            }
            list.closest('li').find('ul').show();
            initStreetList();
        }, 'json');
        return false;
    });
    
    if($.browser.safari) {
        $('a.search-tab.activ').click(function(){

        });
    } else {
        $('a.search-tab.activ').click();
    }
    
    $('.search-tab-drop').on('click', function(){
        $('#search-tab4').toggle();
        $('.search-tab-drop span').toggleClass('open');
    });
    
        
    $('.region-video-list-title').on('click', function(){
        //$('.region-video-list ul').hide();
        $('.'+$(this).data('region')).toggle('slow');
    });
    
}
/*херня для плейсхолдеров не работает 
    $(function(){
    $('#searchRequestForm input[type="text"]').focus(function(){
    $('#searchRequestForm input[type="text"]').attr('value');
    });
    });
    */
function searchTypeChange(form) {
    var data = $('[name="search_type"]').find('option:selected').data();
    if (!data)
        return;
    var placeholder = '';
    if (data.class === 1) {
        placeholder = 'Комнат';
    } else if (data.class === 2) {
        placeholder = 'Тип дома';
    } else if (data.class === 3) {
        placeholder = 'Тип помещения';
    } else if (data.class === 4) {
        placeholder = 'Тип участка';
    }
    if (form) {
        $('#oClass').off();
        $('#oClass').html('<ul></ul>');
        $('#oClass').parent().find('.treecombo-ulWrapper').remove();
        var html_combo = '';
        for (var i in oClasses[data.class]) {
            var class_data = oClasses[data.class][i];
            if (in_array(class_data.id, oClassVal))
                checked = 'checked';
            else
                checked = '';
            html_combo += '<li><label><input type="checkbox" name="oClass[]" value="' + class_data.id + '" ' + checked + '>' + class_data.name + '</label>';
        }
        $('#oClass ul').html(html_combo);
        $('#oClass').treecombo({
            width: '100%',
            comboWidth: '250px',
            comboHeight: '370px',
            'placeholder': placeholder,
            'treeControl': false
        });

        clearAddress();
        $('[name="oClass[]"]').click(function() {
            clearAddress();
        });
    }
    $('#PRICE_CLASS').empty().hide();
    if (data.hidepc !== '1') {
        var price_class_data = price_class({class: data.class, SaleRent: data.sr, term: data.term});
        for (i in price_class_data) {
            if(i == select_price_class) {
                $('#PRICE_CLASS').append('<option selected="selected" value="' + i + '">' + price_class_data[i] + '</option>');
            } else {
                $('#PRICE_CLASS').append('<option value="' + i + '">' + price_class_data[i] + '</option>');
            }
        }
        $('#PRICE_CLASS').show();
    }
}

function in_array(needle, haystack, strict) {
    var found = false, key, strict = !!strict;
    for (key in haystack) {
        if ((strict && haystack[key] === needle) || (!strict && haystack[key] == needle)) {
            found = true;
            break;
        }
    }
    return found;
}



function clearAddress() {
    $('[name="HOUSE_NUM"]').val('');
    $('#house_num').flushCache();
    // not finished
    // сначала сделать фильтр улиц по районам
//    $('#streetSearch').val('');
}

function price_class(type) {
    if (type.class === 4) {
        return {'0': 'Общая', '4': 'За сотку'};
    } else if (type.term === 'day') {
        return {'3': 'Посуточно'};
    } else if (type.class < 3 && type.SaleRent === 'rent') {
        return {'2': 'Помесячно'};
    } else {
        return {'0': 'Общая', '1': 'За метр'};
    }
}

function formTypeChange(isAddEmpty) {
    clearFormInput();
    
    var type = $('[name=search_type]').val();
    $('.showAll').show();
    if (type === 'arenda-kvartir-posutochno')
        $('.saleFlat').hide();
    if (type === 'snyat-dom-posutochno')
        $('.saleHouseDay').hide();
    if (type === 'snyat-kvartiru')
        $('.rentFlat').hide();
    if (type === 'arenda-v-novostrojkax')
        $('.rentNewFlat').hide();
    if (type === 'arenda-elitnyx-kvartir')
        $('.rentVeryNewFlat').hide();
    if (type === 'arenda-domov')
        $('.rentHouse').hide();
    if (type === 'arenda-kommercheskoj')
        $('.rentCom').hide();
    if (type === 'arenda-uchastkov')
        $('.rentEarth').hide();
    if (type === 'kupit-kvartiry')
        $('.buyFlat').hide();
    if (type === 'kvartiry-ot-zastrojshhikov')
        $('.buyFlatHandMade').hide();
    if (type === 'prodazha-v-novostrojkax')
        $('.buyFlatIn').hide();
    if (type === 'prodazha-elitnyx-kvartir')
        $('.saleVip').hide();
    if (type === 'prodazha-domov')
        $('.saleHouse').hide();
    if (type === 'doma-v-kottedzhnyx-gorodkax')
        $('.saleCottage').hide();
    if (type === 'prodazha-kommercheskoj')
        $('.saleCom').hide();
    if (type === 'nezhilye-ot-zastrojshhikov')
        $('.notLive').hide();
    if (type === 'prodazha-uchastkov')
        $('.saleEarth').hide();

    data = $(this).find('option:selected').data();
    $('.shAll').hide();
    realtTypeChange = true;
}



function refineSearch(prm) {
    if (prm === 1) {
        $('.top_search_line').show();
        $('.bottom_search_line').hide();
    } else {
        $('.top_search_line').hide();
        $('.bottom_search_line').show();
        $('#div_extended_find').show();
        $('#extended_search').addClass("nos_top");
        advSearchVisible = true;
    }
    $('.activ form input').focus();
    regionWidth();

}
var tabClick = false;
function indexSearchTab() {
    $('.search-form-tabs a.search-tab').on('click', function() {
        $('.search-form-tabs a').removeClass('activ');
        $(this).addClass('activ');
        $('.search_flat_tab').removeClass('activ');
        tabs = $(this).attr('data-href');
        $('#' + tabs).addClass('activ');
        regionWidth();
            //console.log(tabClick);
        if(tabClick === true) {
            refineSearch();
        }
        tabClick = true;
        return false;
    });
}
function regionWidth() {
    a = $('.activ .region-line').width()-9;
    g = $('.activ .first-line').width();
    r = $('.activ .table-region-house').width();
   
    width = a - g - r;
    //widthh = a - g - r-57;
    $('.activ .second-line').css('width', width);
    //$('.activ .second-line-house').css('width', widthh);
   if (jQuery.browser.msie){
        $('.table-region-for-house table').css('width',455 + 'px');
        $('.table-region-for-house-form-object table').css('width',400 + 'px');
      
        if ( $('.activ .second-line-for-ie').length > 0 ){
        $('.activ .second-line').css('width', width - 10 + 'px');
       }
    } 
 
}
function clearForm() {
    $(':checked').removeAttr('checked');
    $('[type=treecombo]').treecombo('clear');
    $('[type=text]').val('');
    $('select').val('');
    $("[name=search_type] [value='kupit-kvartiry']").attr('selected', 'selected');
    formTypeChange();
}

function clearFormInput() {
    if(realtTypeChange === true) {
        $('[type=text]').val('');
    }
}

function chTab(o) {
    if (typeof sessionStorage)
        sessionStorage.setItem('curTab', $(o).data('href'));
} 


function streetScroll() {
    $('.service-street-scroll a').on('click', function() {
        href = $(this).data('href');
        var top = $('[name=' + href + ']').offset().top - 80;
        //console.log(top);
        $("html, body").animate({
            scrollTop: top
        }, 1000);
        return false;
    });

}

function initStreetList() {
    //var handler = $('.street-list-wrap .liters-list');

    $('.street-list-wrap .liters-list').off().wookmark({
        // Prepare layout options.
        autoResize: true, // This will auto-update the layout when the browser window is resized.
        container: $('.street-list-wrap'), // Optional, used for some extra CSS styling
        offset: 5, // Optional, the distance between grid items
        outerOffset: 10, // Optional, the distance to the containers border
        itemWidth: 210 // Optional, the width of a grid item
    });
}/*! http://mths.be/placeholder v2.0.8 by @mathias */
;(function(window, document, $) {

	// Opera Mini v7 doesn’t support placeholder although its DOM seems to indicate so
	var isOperaMini = Object.prototype.toString.call(window.operamini) == '[object OperaMini]';
	var isInputSupported = 'placeholder' in document.createElement('input') && !isOperaMini;
	var isTextareaSupported = 'placeholder' in document.createElement('textarea') && !isOperaMini;
	var prototype = $.fn;
	var valHooks = $.valHooks;
	var propHooks = $.propHooks;
	var hooks;
	var placeholder;

	if (isInputSupported && isTextareaSupported) {

		placeholder = prototype.placeholder = function() {
			return this;
		};

		placeholder.input = placeholder.textarea = true;

	} else {

		placeholder = prototype.placeholder = function() {
			var $this = this;
			$this
				.filter((isInputSupported ? 'textarea' : ':input') + '[placeholder]')
				.not('.placeholder')
				.bind({
					'focus.placeholder': clearPlaceholder,
					'blur.placeholder': setPlaceholder
				})
				.data('placeholder-enabled', true)
				.trigger('blur.placeholder');
			return $this;
		};

		placeholder.input = isInputSupported;
		placeholder.textarea = isTextareaSupported;

		hooks = {
			'get': function(element) {
				var $element = $(element);

				var $passwordInput = $element.data('placeholder-password');
				if ($passwordInput) {
					return $passwordInput[0].value;
				}

				return $element.data('placeholder-enabled') && $element.hasClass('placeholder') ? '' : element.value;
			},
			'set': function(element, value) {
				var $element = $(element);

				var $passwordInput = $element.data('placeholder-password');
				if ($passwordInput) {
					return $passwordInput[0].value = value;
				}

				if (!$element.data('placeholder-enabled')) {
					return element.value = value;
				}
				if (value == '') {
					element.value = value;
					// Issue #56: Setting the placeholder causes problems if the element continues to have focus.
					if (element != safeActiveElement()) {
						// We can't use `triggerHandler` here because of dummy text/password inputs :(
						setPlaceholder.call(element);
					}
				} else if ($element.hasClass('placeholder')) {
					clearPlaceholder.call(element, true, value) || (element.value = value);
				} else {
					element.value = value;
				}
				// `set` can not return `undefined`; see http://jsapi.info/jquery/1.7.1/val#L2363
				return $element;
			}
		};

		if (!isInputSupported) {
			valHooks.input = hooks;
			propHooks.value = hooks;
		}
		if (!isTextareaSupported) {
			valHooks.textarea = hooks;
			propHooks.value = hooks;
		}

		$(function() {
			// Look for forms
			$(document).delegate('form', 'submit.placeholder', function() {
				// Clear the placeholder values so they don't get submitted
				var $inputs = $('.placeholder', this).each(clearPlaceholder);
				setTimeout(function() {
					$inputs.each(setPlaceholder);
				}, 10);
			});
		});

		// Clear placeholder values upon page reload
		$(window).bind('beforeunload.placeholder', function() {
			$('.placeholder').each(function() {
				this.value = '';
			});
		});

	}

	function args(elem) {
		// Return an object of element attributes
		var newAttrs = {};
		var rinlinejQuery = /^jQuery\d+$/;
		$.each(elem.attributes, function(i, attr) {
			if (attr.specified && !rinlinejQuery.test(attr.name)) {
				newAttrs[attr.name] = attr.value;
			}
		});
		return newAttrs;
	}

	function clearPlaceholder(event, value) {
		var input = this;
		var $input = $(input);
		if (input.value == $input.attr('placeholder') && $input.hasClass('placeholder')) {
			if ($input.data('placeholder-password')) {
				$input = $input.hide().next().show().attr('id', $input.removeAttr('id').data('placeholder-id'));
				// If `clearPlaceholder` was called from `$.valHooks.input.set`
				if (event === true) {
					return $input[0].value = value;
				}
				$input.focus();
			} else {
				input.value = '';
				$input.removeClass('placeholder');
				input == safeActiveElement() && input.select();
			}
		}
	}

	function setPlaceholder() {
		var $replacement;
		var input = this;
		var $input = $(input);
		var id = this.id;
		if (input.value == '') {
			if (input.type == 'password') {
				if (!$input.data('placeholder-textinput')) {
					try {
						$replacement = $input.clone().attr({ 'type': 'text' });
					} catch(e) {
						$replacement = $('<input>').attr($.extend(args(this), { 'type': 'text' }));
					}
					$replacement
						.removeAttr('name')
						.data({
							'placeholder-password': $input,
							'placeholder-id': id
						})
						.bind('focus.placeholder', clearPlaceholder);
					$input
						.data({
							'placeholder-textinput': $replacement,
							'placeholder-id': id
						})
						.before($replacement);
				}
				$input = $input.removeAttr('id').hide().prev().attr('id', id).show();
				// Note: `$input[0] != input` now!
			}
			$input.addClass('placeholder');
			$input[0].value = $input.attr('placeholder');
		} else {
			$input.removeClass('placeholder');
		}
	}

	function safeActiveElement() {
		// Avoid IE9 `document.activeElement` of death
		// https://github.com/mathiasbynens/jquery-placeholder/pull/99
		try {
			return document.activeElement;
		} catch (exception) {}
	}

}(this, document, jQuery));