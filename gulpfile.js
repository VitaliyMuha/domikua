'use strict';

var gulp = require('gulp'),
    watch = require('gulp-watch'),
    prefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    rigger = require('gulp-rigger'),
    cssmin = require('gulp-cssnano'),
    imagemin = require('gulp-imagemin'),
    pngquant = require('imagemin-pngquant')//,
    //rimraf = require('rimraf'),
    /*browserSync = require("browser-sync"),
    reload = browserSync.reload*/;

gulp.task('sass', function () {
    gulp.src('src/style/*.scss')
        //.pipe(sass().on('error', sass.logError))
        .pipe(sourcemaps.init())
        .pipe(prefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('111/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('src/style/*.scss', ['sass']);
});

gulp.task('default', ['sass:watch']);